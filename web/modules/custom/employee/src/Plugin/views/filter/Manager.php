<?php

namespace Drupal\employee\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;

/**
 * @ViewsFilter("manager_filter")
 */
class Manager extends FilterPluginBase implements ContainerFactoryPluginInterface {
  private $database;
  /**
   * Constructor
   *
   * @param array $configuration
   * @param [type] $plugin_id
   * @param [type] $plugin_definition
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $database) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database')
    );
  }
  /**
   * {@inheritdoc}
   */
 
  public function query() {
       
  $query = $this->database->select('node_field_data', 'd');
 $query->fields('d', ['nid', 'title']);
 $query->addField('m', 'field_manager_id_value');
 $query->addJoin('node__field_manager_id', 'm', 'm.entity_id = d.nid');
 $results = $query->execute()->fetchAll();
  
}
}
  

