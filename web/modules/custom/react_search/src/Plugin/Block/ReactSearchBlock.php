<?php
namespace Drupal\react_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'ReactSearchBlock' block.
 *
 * @Block(
 *  id = "react_search_block",
 *  admin_label = @Translation("React Search block"),
 * )
 */
class ReactSearchBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['react_search_block'] = [
      '#markup' => '<div id="react-app"></div>',
      '#attached' => [
        'library' => 'react_search/react_app'
      ],
    ];
    return $build;
  }
}