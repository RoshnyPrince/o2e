import React from 'react'
import ReactDOM from 'react-dom'
import EmployeeList from "./components/EmployeeList";

const Main = () => (
  <EmployeeList />
);

ReactDOM.render(<Main/>, document.getElementById('react-app'));