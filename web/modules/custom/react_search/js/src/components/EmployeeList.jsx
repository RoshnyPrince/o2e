import React, { useState } from "react";

function SearchEmployee() {
  const [searchTerm, setSearchTerm] = useState("");
  const [employees, setEmployees] = useState([]);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch(`http://o2e.ddev.site/jsonapi/node/employee?title=${searchTerm}`);
    const data = await response.json();
    setEmployees(data);
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          value={searchTerm}
          onChange={(event) => setSearchTerm(event.target.value)}
        />
        <button type="submit">Search</button>
      </form>
      <ul>
        {employees.map((employee) => (
          <li key={employee.id}>{employee.title}</li>
        ))}
      </ul>
    </div>
  );
}

export default EmployeeList;
